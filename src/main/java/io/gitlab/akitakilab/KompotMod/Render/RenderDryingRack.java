package io.gitlab.akitakilab.KompotMod.Render;

import com.bioxx.tfc.api.TFCBlocks;
import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import io.gitlab.akitakilab.KompotMod.ModManager;
import io.gitlab.akitakilab.KompotMod.TileEntities.TEDryingRack;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import org.lwjgl.opengl.GL11;


public class RenderDryingRack implements ISimpleBlockRenderingHandler {
	@Override
	public int getRenderId() {
		return ModManager.dryingRackRenderID;
	}

	@Override
	public boolean shouldRender3DInInventory(int modelId) {
		return true;
	}

	@Override
	public boolean renderWorldBlock(IBlockAccess world, int i, int j, int k,
									Block block, int modelId,
									RenderBlocks renderer) {
		TileEntity entity = world.getTileEntity(i, j, k);
		if (!(entity instanceof TEDryingRack))
			return false;

		TEDryingRack te = (TEDryingRack) entity;
		Block planks = TFCBlocks.planks;

		// Render the four strips of wood planks
		renderer.setRenderBounds(0.025F, 0.025F, 0.025F, 0.225F, 0.05F, 0.975F);
		renderer.renderStandardBlock(planks, i, j, k);

		renderer.setRenderBounds(0.275F, 0.025F, 0.025F, 0.475F, 0.05F, 0.975F);
		renderer.renderStandardBlock(planks, i, j, k);

		renderer.setRenderBounds(0.525F, 0.025F, 0.025F, 0.725F, 0.05F, 0.975F);
		renderer.renderStandardBlock(planks, i, j, k);

		renderer.setRenderBounds(0.775F, 0.025F, 0.025F, 0.975F, 0.05F, 0.975F);
		renderer.renderStandardBlock(planks, i, j, k);

		// Render the ropes
		renderer.setRenderBounds(0, 0, 0.15F, 1, 0.075F, 0.175F);
		renderer.renderStandardBlock(TFCBlocks.thatch, i, j, k);

		renderer.setRenderBounds(0, 0, 0.4875F, 1, 0.075F, 0.5125F);
		renderer.renderStandardBlock(TFCBlocks.thatch, i, j, k);

		renderer.setRenderBounds(0, 0, 0.85F, 1, 0.075F, 0.875F);
		renderer.renderStandardBlock(TFCBlocks.thatch, i, j, k);

		// Render the item in inventory if needed
		if (te.itemSlot != null) {
			if (te.itemSlot.getItem() == ModManager.wetAshItem) {
				renderer.setRenderBounds(0.1F, 0.05F, 0.1F, 0.9F, 0.15F, 0.9F);
				renderer.renderStandardBlock(ModManager.wetAshBlock, i, j, k);
			}

			if (te.itemSlot.getItem() == ModManager.potashLyeItem) {
				renderer.setRenderBounds(0.1F, 0.05F, 0.1F, 0.9F, 0.10F, 0.9F);
				renderer.renderStandardBlock(ModManager.dryAshBlock, i, j, k);
			}
		}

		return true;
	}

	@Override
	public void renderInventoryBlock(Block block, int metadata, int modelID,
			RenderBlocks renderer) {
		Block planks = TFCBlocks.planks;
		Block thatch = TFCBlocks.thatch;

		// Render the four strips of wood planks
		renderer.setRenderBounds(0.025F, 0.358F, 0.025F, 0.225F, 0.383F, 0.975F);
		renderInvBlock(planks, metadata, renderer);

		renderer.setRenderBounds(0.275F, 0.358F, 0.025F, 0.475F, 0.383F, 0.975F);
		renderInvBlock(planks, metadata, renderer);

		renderer.setRenderBounds(0.525F, 0.358F, 0.025F, 0.725F, 0.383F, 0.975F);
		renderInvBlock(planks, metadata, renderer);

		renderer.setRenderBounds(0.775F, 0.358F, 0.025F, 0.975F, 0.383F, 0.975F);
		renderInvBlock(planks, metadata, renderer);

		// Render the ropes
		renderer.setRenderBounds(0, 0.333, 0.15F, 1, 0.408F, 0.175F);
		renderInvBlock(thatch, metadata, renderer);

		renderer.setRenderBounds(0, 0.333, 0.4875F, 1, 0.408F, 0.5125F);
		renderInvBlock(thatch, metadata, renderer);

		renderer.setRenderBounds(0, 0.333, 0.85F, 1, 0.408F, 0.875F);
		renderInvBlock(thatch, metadata, renderer);
	}

	public static void renderInvBlock(Block block, int m, RenderBlocks renderer) {
		Tessellator tes = Tessellator.instance;
		GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
		tes.startDrawingQuads();
		tes.setNormal(0.0F, -1.0F, 0.0F);
		renderer.renderFaceYNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(0, m));
		tes.draw();
		tes.startDrawingQuads();
		tes.setNormal(0.0F, 1.0F, 0.0F);
		renderer.renderFaceYPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(1, m));
		tes.draw();
		tes.startDrawingQuads();
		tes.setNormal(0.0F, 0.0F, -1.0F);
		renderer.renderFaceXNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(2, m));
		tes.draw();
		tes.startDrawingQuads();
		tes.setNormal(0.0F, 0.0F, 1.0F);
		renderer.renderFaceXPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(3, m));
		tes.draw();
		tes.startDrawingQuads();
		tes.setNormal(-1.0F, 0.0F, 0.0F);
		renderer.renderFaceZNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(4, m));
		tes.draw();
		tes.startDrawingQuads();
		tes.setNormal(1.0F, 0.0F, 0.0F);
		renderer.renderFaceZPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(5, m));
		tes.draw();
		GL11.glTranslatef(0.5F, 0.5F, 0.5F);
	}
}
