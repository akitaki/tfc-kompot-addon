package io.gitlab.akitakilab.KompotMod;

/**
 * Proxy for client-side only
 */
public class ClientProxy extends CommonProxy {
	@Override
	public boolean isRemote() {
		return true;
	}
}
