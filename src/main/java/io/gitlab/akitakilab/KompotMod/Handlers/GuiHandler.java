package io.gitlab.akitakilab.KompotMod.Handlers;

import cpw.mods.fml.common.network.IGuiHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import io.gitlab.akitakilab.KompotMod.Containers.ContainerGlassContainer;
import io.gitlab.akitakilab.KompotMod.Gui.GuiGlassContainer;


public class GuiHandler implements IGuiHandler {
	public static final int glassContainerGuiId = 0;

	@Override
	public Object getServerGuiElement(int Id, EntityPlayer player, World world, int x, int y, int z) {
        if (Id == glassContainerGuiId) {
			return new ContainerGlassContainer(player.inventory, world, x, y, z);
        }
        return null;
    }

	@Override
	public Object getClientGuiElement(int Id, EntityPlayer player, World world, int x, int y, int z) {
		if (Id == glassContainerGuiId) {
			return new GuiGlassContainer(player.inventory, world, x, y, z);
		}
		return null;
	}
}
