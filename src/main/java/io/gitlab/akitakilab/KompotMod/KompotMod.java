package io.gitlab.akitakilab.KompotMod;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;


@Mod(modid = KompotMod.MODID, version = KompotMod.VERSION)
public class KompotMod {
	public static final String MODID = "kompotmod";
	public static final String VERSION = "1.0";

	@Instance(MODID)
	public static KompotMod ins;

	@SidedProxy(
				clientSide = "io.gitlab.akitakilab.KompotMod.ClientProxy",
				serverSide = "io.gitlab.akitakilab.Kompotmod.CommonProxy")
	public static CommonProxy proxy;

	@EventHandler
	public void init(FMLInitializationEvent event) {
		proxy.initJobs();
		ModManager.setupLogger();
		ModManager.getRenderIDs();
		ModManager.loadBlocks();
		ModManager.loadItems();
		ModManager.registerTileEntities();
		ModManager.registerBlocks();
		ModManager.registerItems();
		ModManager.registerRenderers();
		ModManager.registerHeatIndexes();
		ModManager.registerBarrelRecipes();
		ModManager.registerRecipes();
		ModManager.registerGuiHandler();
	}
}
