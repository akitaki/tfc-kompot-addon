package io.gitlab.akitakilab.KompotMod.Containers;

import com.bioxx.tfc.Containers.ContainerTFC;
import com.bioxx.tfc.Containers.Slots.SlotChest;
import io.gitlab.akitakilab.KompotMod.TileEntities.TEGlassCan;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.world.World;


public class ContainerGlassCan extends ContainerTFC {
	public TEGlassCan te;

	public ContainerGlassCan(InventoryPlayer p, TEGlassCan te, World w, int x, int y, int z) {
		this.te = te;
	}

	private void buildLayout() {
		this.addSlotToContainer(new SlotChest(te, 0, 80, 29));
	}

	@Override
	public boolean canInteractWith(EntityPlayer entityplayer) {
		return true;
	}
}
