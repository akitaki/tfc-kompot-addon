package io.gitlab.akitakilab.KompotMod.Items;

import com.bioxx.tfc.Items.Pottery.ItemPotteryBase;
import com.bioxx.tfc.api.Enums.EnumWeight;
import com.bioxx.tfc.api.Enums.EnumSize;
import net.minecraft.util.IIcon;
import cpw.mods.fml.relauncher.SideOnly;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.client.renderer.texture.IIconRegister;
import io.gitlab.akitakilab.KompotMod.KompotMod;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraft.entity.player.EntityPlayer;
import io.gitlab.akitakilab.KompotMod.ModManager;


public class ItemGlassContainer extends ItemPotteryBase {
	public ItemGlassContainer() {
		super();
		this.setMaxStackSize(1);
		this.metaNames = new String[]{"ClayGlassContainer", "CeramicGlassContainer"};
		this.setWeight(EnumWeight.MEDIUM);
		this.setSize(EnumSize.SMALL);
	}

	@Override
	public void registerIcons(IIconRegister registerer) {
		clayIcon = registerer.registerIcon(KompotMod.MODID + ":" + metaNames[0]);
		ceramicIcon = registerer.registerIcon(KompotMod.MODID + ":" + metaNames[1]);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconFromDamage(int damage) {
		if(damage == 0)
			return this.clayIcon;
		else
			return this.ceramicIcon;
	}

	@Override
	public ItemStack onItemRightClick(ItemStack is, World world, EntityPlayer ep) {
		ModManager.info("ItemGlassContainer#onItemRightClick");
		if (!ep.isSneaking() && is.getItemDamage() == 1) {
			ModManager.info("Opening the GUI");
			ep.openGui(KompotMod.MODID, ModManager.guiHandler.glassContainerGuiId, world, (int) ep.posX, (int) ep.posY, (int) ep.posZ);
		}
		return is;
	}
}
