package io.gitlab.akitakilab.KompotMod;

import com.bioxx.tfc.Core.TFCTabs;
import com.bioxx.tfc.api.Constant.Global;
import com.bioxx.tfc.api.Crafting.BarrelManager;
import com.bioxx.tfc.api.Crafting.BarrelRecipe;
import com.bioxx.tfc.api.HeatIndex;
import com.bioxx.tfc.api.HeatRegistry;
import com.bioxx.tfc.api.TFCBlocks;
import com.bioxx.tfc.api.TFCFluids;
import com.bioxx.tfc.api.TFCItems;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import io.gitlab.akitakilab.KompotMod.KompotMod;
import io.gitlab.akitakilab.KompotMod.Blocks.BlockDryAsh;
import io.gitlab.akitakilab.KompotMod.Blocks.BlockDryingRack;
import io.gitlab.akitakilab.KompotMod.Blocks.BlockGlassCan;
import io.gitlab.akitakilab.KompotMod.Blocks.BlockWetAsh;
import io.gitlab.akitakilab.KompotMod.Items.ItemDryingRack;
import io.gitlab.akitakilab.KompotMod.Items.ItemGlassCan;
import io.gitlab.akitakilab.KompotMod.Items.ItemKompot;
import io.gitlab.akitakilab.KompotMod.Render.RenderDryingRack;
import io.gitlab.akitakilab.KompotMod.Render.RenderGlassCan;
import io.gitlab.akitakilab.KompotMod.TileEntities.TEDryingRack;
import io.gitlab.akitakilab.KompotMod.TileEntities.TEGlassCan;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.oredict.OreDictionary;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import io.gitlab.akitakilab.KompotMod.Blocks.BlockBronze;
import io.gitlab.akitakilab.KompotMod.Items.ItemGlassContainer;
import cpw.mods.fml.common.ModAPIManager;
import io.gitlab.akitakilab.KompotMod.Handlers.GuiHandler;
import cpw.mods.fml.common.network.NetworkRegistry;


public class ModManager {
	/** Blocks */
	public static Block glassCanBlock;
	public static Block dryingRackBlock;

	/** Internal blocks, only intended for rendering purpose */
	public static Block wetAshBlock;
	public static Block dryAshBlock;
	public static Block bronzeBlock;

	/** Items */
	public static Item glassCanItem;
	public static Item dryingRackItem;
	public static Item ashItem;
	public static Item wetAshItem;
	public static Item potashLyeItem;
	public static Item glassContainerItem;

	/** Renderers */
	public static int glassCanBlockRenderID;
	public static int dryingRackRenderID;

	/** Handlers */
	public static GuiHandler guiHandler;

	/** Logger */
	private static Logger log;

	public static void setupLogger() {
		log = LogManager.getLogger(KompotMod.MODID);
	}

	public static void getRenderIDs() {
		glassCanBlockRenderID = RenderingRegistry.getNextAvailableRenderId();
		dryingRackRenderID = RenderingRegistry.getNextAvailableRenderId();
	}

	public static void loadBlocks() {
		glassCanBlock = new BlockGlassCan()
			.setBlockName("GlassCan")
			.setHardness(4)
			.setResistance(4);
		dryingRackBlock = new BlockDryingRack()
			.setBlockName("DryingRack")
			.setHardness(4)
			.setResistance(4);
		wetAshBlock = new BlockWetAsh();
		dryAshBlock = new BlockDryAsh();
		bronzeBlock = new BlockBronze();
	}

	public static void loadItems() {
		ashItem = new ItemKompot()
			.setFolder("materials/")
			.setUnlocalizedName("Ash")
			.setCreativeTab(TFCTabs.TFC_MATERIALS);
		potashLyeItem = new ItemKompot()
			.setFolder("materials/")
			.setUnlocalizedName("PotashLye")
			.setCreativeTab(TFCTabs.TFC_MATERIALS);
		wetAshItem = new ItemKompot()
			.setFolder("materials/")
			.setUnlocalizedName("WetAsh")
			.setCreativeTab(TFCTabs.TFC_MATERIALS);
		glassContainerItem = new ItemGlassContainer()
			.setUnlocalizedName("GlassContainer")
			.setCreativeTab(TFCTabs.TFC_POTTERY);
	}

	public static void registerTileEntities() {
		GameRegistry.registerTileEntity(TEGlassCan.class, "TEGlassCan");
		GameRegistry.registerTileEntity(TEDryingRack.class, "TEDryingRack");
	}

	public static void registerBlocks() {
		GameRegistry.registerBlock(glassCanBlock, ItemGlassCan.class, "GlassCan");
		GameRegistry.registerBlock(dryingRackBlock, ItemDryingRack.class,
								   "DryingRack");
		GameRegistry.registerBlock(wetAshBlock, "WetAsh");
		GameRegistry.registerBlock(dryAshBlock, "DryAsh");
		GameRegistry.registerBlock(bronzeBlock, "Bronze");
	}

	public static void registerItems() {
		GameRegistry.registerItem(ashItem, "Plant Ash");
		GameRegistry.registerItem(potashLyeItem, "Potash Lye");
		GameRegistry.registerItem(wetAshItem, "Wet Ash");
		GameRegistry.registerItem(glassContainerItem, "Glass Container");
	}

	public static void registerRenderers() {
		RenderingRegistry.registerBlockHandler(new RenderGlassCan());
		RenderingRegistry.registerBlockHandler(new RenderDryingRack());
	}

	/** Register the heat indexes that's used in TFC */
	public static void registerHeatIndexes() {
		HeatRegistry manager = HeatRegistry.getInstance();
		int WILDCARD_VALUE = OreDictionary.WILDCARD_VALUE;

		// For reference:
		// public HeatIndex(ItemStack in, double sh, double melt, ItemStack out)
		manager.addIndex(new HeatIndex(new ItemStack(TFCBlocks.thatch, 1, WILDCARD_VALUE), 0.75,
									   400, new ItemStack(ashItem, 1)));
	}

	public static void registerBarrelRecipes() {
		BarrelRecipe ashRecipe =
			new BarrelRecipe(
							 new ItemStack(ashItem, 8), new FluidStack(TFCFluids.LIMEWATER, 75),
							 new ItemStack(wetAshItem, 2), new FluidStack(TFCFluids.LIMEWATER, 75),
							 1);

		ashRecipe.setAllowAnyStack(true);
		BarrelManager.getInstance().addRecipe(ashRecipe);
	}

	public static void registerKilnRecipes() {}

	public static void registerGuiHandler() {
		guiHandler = new GuiHandler();
		NetworkRegistry.INSTANCE.registerGuiHandler(KompotMod.ins, guiHandler);
	}

	public static void registerKnappingRecipes() {
		// 	CraftingManagerTFC.getInstance().addRecipe(new
		// ItemStack(TFCBlocks.vessel, 1), new Object[]
		// {
		// 		"     ",
		// 		" ### ",
		// 		" ### ",
		// 		" ### ",
		// 		"     ", '#', new ItemStack(TFCItems.flatClay, 1, 1)});
	}

	public static void registerRecipes() {
		// For each kind of TFC wood
		for(int i = 0; i < Global.WOOD_ALL.length; i++) {
			GameRegistry.addRecipe(new ItemStack(dryingRackBlock, 1, i),
								   "/|/",
								   "#|#",
								   "/|/",
								   '/', TFCItems.stick,
								   '|', new ItemStack(TFCItems.singlePlank, 1, i),
								   '#', TFCItems.juteFiber);
		}
	}

	public static void info(String msg) { log.info("[KompotMod] " + msg); }
	
	public static void warn(String msg) { log.warn("[KompotMod] " + msg); }
}
