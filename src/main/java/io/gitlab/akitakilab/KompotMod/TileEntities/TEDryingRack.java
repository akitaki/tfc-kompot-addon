package io.gitlab.akitakilab.KompotMod.TileEntities;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import io.gitlab.akitakilab.KompotMod.ModManager;
import com.bioxx.tfc.TileEntities.NetworkTileEntity;
import net.minecraft.item.Item;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderGlobal;
import com.bioxx.tfc.Core.TFC_Time;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.Entity;

public class TEDryingRack extends NetworkTileEntity implements IInventory {
	public ItemStack itemSlot;
	private boolean drying;
	private long placedTime;
	
	public TEDryingRack() {
	}

	@Override
	public void updateEntity() {
		if (worldObj.isRemote)
			return;

		long time = TFC_Time.getTotalHours() - placedTime;
		if (drying && time >= 12) {
			// Dried
			setInventorySlotContents(0, new ItemStack(ModManager.potashLyeItem, 8));
			drying = false;
		}
	}

	public void careForInventorySlot() {
		// TODO: Update content status
	}

	@Override
	public void readFromNBT(NBTTagCompound tagComp) {
		super.readFromNBT(tagComp);

		NBTTagCompound itemTagComp = tagComp.getCompoundTag("Item");
		itemSlot = ItemStack.loadItemStackFromNBT(itemTagComp);

		drying = tagComp.getBoolean("Drying");
		placedTime = tagComp.getLong("PlacedTime");
	}

	@Override
	public void writeToNBT(NBTTagCompound tagComp) {
		super.writeToNBT(tagComp);

		NBTTagCompound itemTagComp = new NBTTagCompound();
		if (itemSlot != null)
			itemSlot.writeToNBT(itemTagComp);
		tagComp.setTag("Item", itemTagComp);

		tagComp.setBoolean("Drying", drying);
		tagComp.setLong("PlacedTime", placedTime);
	}

	@Override
	public void createInitNBT(NBTTagCompound tagComp) {
		NBTTagCompound itemTagComp = new NBTTagCompound();
		if (itemSlot != null)
			itemSlot.writeToNBT(itemTagComp);
		tagComp.setTag("Item", itemTagComp);
	}

	@Override
	public void handleInitPacket(NBTTagCompound tagComp) {
		NBTTagCompound itemTagComp = tagComp.getCompoundTag("Item");
		itemSlot = ItemStack.loadItemStackFromNBT(itemTagComp);
		drying = tagComp.getBoolean("Drying");
		placedTime = tagComp.getLong("PlacedTime");
		worldObj.func_147479_m(xCoord, yCoord, zCoord);
	}

	@Override
	public int getInventoryStackLimit() {
		return 1;
	}

	@Override
	public int getSizeInventory() {
		return 1;
	}

	@Override
	public boolean isItemValidForSlot(int slot, ItemStack is) {
		return true;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player) {
		return true;
	}

	@Override
	public boolean hasCustomInventoryName() {
		return true;
	}

	@Override
	public String getInventoryName() {
		return "Drying Rack";
	}

	@Override
	public void setInventorySlotContents(int slot, ItemStack is) {
		if (slot != 0)
			return;

		itemSlot = is;

		if (is != null && is.getItem() == ModManager.wetAshItem) {
			placedTime = TFC_Time.getTotalHours();
			drying = true;
		}

		worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
		this.markDirty();
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int slot) {
		return null;
	}

	@Override
	public ItemStack decrStackSize(int slot, int amount) {
		ItemStack stack = itemSlot;
		if (stack != null) {
			if (stack.stackSize <= amount) {
				setInventorySlotContents(0, null);
			} else {
				stack = stack.splitStack(amount);
			}
		}
		return stack;
	}

	@Override
	public ItemStack getStackInSlot(int slot) {
		return itemSlot;
	}

	@Override
	public void openInventory() {
	}

	@Override
	public void closeInventory() {
	}

	/**
	 * Determine whether the rack can accept part of an ItemStack
	 */
	public boolean acceptsItem(ItemStack is) {
		if (itemSlot == null) {
			// Slot empty state
			if (is.getItem() == ModManager.wetAshItem && is.stackSize >= 8) {
				// Accepts 8 wet ash
				return true;
			}
		}
		return false;
	}

	/**
	 * Add part of the ItemStack to the drying rack.
	 * @param is The stack to add
	 * @return The stack after manipulation. Might be null.
	 */
	public ItemStack add(ItemStack is) {
		if (is.getItem() == ModManager.wetAshItem) {
			setInventorySlotContents(0, new ItemStack(is.getItem(), 8));
			is.stackSize -= 8;
		}
		return is;
	}

	/**
	 * Drops the ItemStack in itemSlot if it's not empty.
	 * Used when the block is clicked by a player without holding an item.
	 */
	public void dropItemIfNotEmpty() {
		if (itemSlot != null) {
			Entity e = new EntityItem(worldObj, xCoord + 0.5, yCoord + 0.5, zCoord + 0.5, itemSlot);
			worldObj.spawnEntityInWorld(e);
			setInventorySlotContents(0, null);
		}
	}
}
